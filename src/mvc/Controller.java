package mvc;
/**
 * The Controller class runs the bridge between the view and the model, accessing
 * both to run the game smoothly.
 * 
 * @author Tierney Irwin
 * 
 * Due Date: 31 March 2016
 */
import java.awt.Image;
import java.awt.Toolkit;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import model.Card;
import model.ComputerPlayer;
import model.Player;
import model.PokerHandRanking;
import model.PokerModel;

public class Controller 
{
	public PokerModel myModel;
	public View myView;
	private boolean myGameRunning = false;
	public boolean myCompTurn = false;
	private ComputerPlayer myComputer;
	private PokerHandRanking myHandRanking;
	private Player myWinner;
	private Player myLoser;

	/**
	 * Main Method runs the entire Poker program.
	 * 
	 * @author Tierney Irwin
	 */
	public static void main(String[] args)
    {
        new Controller();
    }
	/**
	 * Constructor sets up a general poker game.
	 * 
	 * @author Tierney Irwin
	 */
	public Controller()
	{
		Player myHuman = new Player(playerNamePane());
		myModel = new PokerModel(myHuman);
		myComputer = (ComputerPlayer) myModel.getPlayer(1);
		myModel.dealCards();
		myModel.getPlayer(0).getHand().orderCards();
		myModel.getPlayer(1).getHand().orderCards();
		myView = new View(this);
		
	}
	
	/**
	 * Second Constructor sets up a poker game with a given view.
	 * 
	 * @param view class setting up the look of the program
	 */
	public Controller(View view)
	{
		Player myHuman = new Player(playerNamePane());
		myModel = new PokerModel(myHuman);
		myComputer = (ComputerPlayer) myModel.getPlayer(1);
		myModel.dealCards();
		myModel.getPlayer(0).getHand().orderCards();
		myModel.getPlayer(1).getHand().orderCards();
		myView = view;
	}
	
/**
 * Method activates the toggleSelected method in the model 
 * based on the amount of cards already selected and the card 
 * you are attempting to select.
 * 
 * @param index Integer value of card selected
 * 
 * @author Tierney Irwin
 */
	
	public void toggleSelected(Integer index)
	{
		int myIndex = index.intValue();
		int count=0;
		if(myGameRunning)
		{
			for(int i=0;i<5;i++)
			{
				if(myModel.getPlayer(0).getHand().getCards().get(i).isSelected()==true)
				{
					count++;
				}
			}
			if(count>=3)
			{
				if(myModel.getPlayer(0).getHand().getCards().get(myIndex).isSelected()==false)
				{
					
				}
				else
				{
					myModel.getPlayer(0).getHand().getCards().get(myIndex).toggleSelected();
					myView.toggleImage(myIndex,myModel.getPlayer(0).getHand().getCards().get(myIndex).isSelected());
				}
			}
			else
			{
				myModel.getPlayer(0).getHand().getCards().get(myIndex).toggleSelected();
				myView.toggleImage(myIndex,myModel.getPlayer(0).getHand().getCards().get(myIndex).isSelected());
			}
		}

	}
	
	/**
	 * Method determines which method, either discard or startGame, 
	 * is called, depending on if the game is currently running.
	 * 
	 * @author Tierney Irwin
	 */
	public void startOrDiscard()
	{
		if(myGameRunning)
		{
			discard();
			myHandRanking = myModel.getPlayer(0).getHand().determineRanking();
			myView.setLog(myView.myLogHandRanking, "Your Hand is: "+myHandRanking.name());
		}
		else
		{
			startGame();
			myHandRanking = myModel.getPlayer(0).getHand().determineRanking();
			myView.setLog(myView.myLogHandRanking, "Your Hand is: "+myHandRanking.name());
		}
	}
	/**
	 * Method initiates the cards for each player and 
	 * flips cards appropriately on whose hand should be showing.
	 * Finally the method changes the title of the main button.
	 * 
	 * @author Tierney Irwin
	 */
	public void startGame()
	{
			myModel.dealCards();
			myModel.getPlayer(0).getHand().orderCards();
			myModel.getPlayer(1).getHand().orderCards();
			boolean myComputerFaceUp=false;
			for(int i=0;i<5;i++)
			{
				if(myModel.getPlayer(1).getHand().getCards().get(i).isFaceUp())
				{
					myComputerFaceUp=myComputerFaceUp&&true;
				}
				
			}
			if(myComputerFaceUp)
			{
				myView.shownCompCards();
			}
			myView.shownCards();
			myGameRunning=true;
			myView.setTitle("Discard");
	}
	/**
	 * Method discards cards selected by the user based on 
	 * those toggled previously and deals them new cards 
	 * to replace the discarded. Finally, the method switches 
	 * the turns and allows the computer player to have their turn,
	 * promptly ending the round afterward.
	 * 
	 * @author Tierney Irwin
	 */
	public void discard()
	{
		myCompTurn=false;
		Vector<Card> discards = myModel.getPlayer(0).getHand().discard();
		myView.setLog(myView.myLog,myModel.getPlayer(0).getName()+" removed "+discards.size()+ " cards.");
		myModel.dealCards();
		myModel.getPlayer(0).getHand().orderCards();
		myHandRanking = myModel.getPlayer(0).getHand().determineRanking();
		myView.setLog(myView.myLogHandRanking, "Your Hand is: "+myHandRanking.name());
		myView.shownCards();
		myModel.switchTurns();
		computerTurn();
		endRound();
	}
	
	/**
	 * Method allows the computer to discard cards it deems unnecessary 
	 * and deals new cards to their hand.
	 * 
	 *@author Tierney Irwin
	 */
	public void computerTurn()
	{
		myCompTurn=true;
		Vector<Integer> myCompDiscards = myComputer.selectCardsToDiscard();
		myView.setLog(myView.mySecondUpdateLog, myModel.getPlayer(1).getName()+" removed "+myCompDiscards.size()+ " cards.");
		myModel.getPlayer(1).getHand().discard(myCompDiscards);
		myModel.dealCards();
		myModel.getPlayer(1).getHand().orderCards();
	}
	
	/**
	 * Method ends the round by determining round winner 
	 * and displaying such in the view. The method
	 * reveals the computer's cards to the user
	 * and determines whether the game fully concluded
	 * based on number of wins each player has.
	 * 
	 * @author Tierney Irwin
	 */
	public void endRound()
	{
		myWinner = myModel.determineWinner();
		if(myWinner == myModel.getPlayer(0))
		{
			myLoser = myModel.getPlayer(1);
		}
		else
		{
			myLoser = myModel.getPlayer(0);
		}
		myView.shownCompCards();
		myView.setLog(myView.myPlayer, myModel.getPlayer(0).getName()+": "+myModel.getPlayer(0).getNumberWins());
		myView.setLog(myView.myComputer, myModel.getPlayer(1).getName()+": "+myModel.getPlayer(1).getNumberWins());
		
		if(myModel.getPlayer(0).getNumberWins()==3)
		{
			endGame(0);	
		}
		else if(myModel.getPlayer(1).getNumberWins()==3)
		{
			
			endGame(1);
		}
		else
		{
			playAgain();
		}
	}
	
	/**
	 * Method reveals a JOptionPane that asks the user if
	 * they would continue the game or not. If yes,
	 * the method resets the game and calls upon startGame to 
	 * initiate the next round. If no, the game ends.
	 * 
	 * @author Tierney Irwin
	 */
	public void playAgain()
	{
			int reply = JOptionPane.showConfirmDialog(null,  myWinner.getName()+" won with a "+ myWinner.getHand().determineRanking().name()+"\n"+ myLoser.getName()+" had a "+ myLoser.getHand().determineRanking().name()+". \nAnother Round?", "Play Again?", JOptionPane.YES_NO_OPTION);
	        if (reply == JOptionPane.YES_OPTION) {
	        	myView.shownCompCards();
	    		myModel.resetGame();
	    		myCompTurn=false;
	    		myGameRunning=false;
	    		startGame();
	    		
	        }
	        else 
	        {
	           JOptionPane.showMessageDialog(null, "Well, Good Riddance","Get Out.",JOptionPane.INFORMATION_MESSAGE);
	           System.exit(0);
	        }
	}
	
	/**
	 * Method ends the game fully after a player gets to three round wins.
	 * Displays a JOptionPane that displays the game winner.
	 * 
	 * @param index int index of player who won the game
	 * 
	 * @author Tierney Irwin
	 */
	public void endGame(int index)
	{
		JOptionPane.showMessageDialog(null, myModel.getPlayer(index).getName()+" is the winner!","WE HAVE A WINNER!",JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
	/**
	 * Method asks the user for a name to be inputed 
	 * that gives the model a name for the user player.
	 * 
	 * @return String name of user
	 * 
	 * @author Tierney Irwin
	 */
	public static String playerNamePane()
	{
		String name = JOptionPane.showInputDialog(null,"Gotta name?","Do you wish to Game?",JOptionPane.INFORMATION_MESSAGE);
		return name;
	}
	
	/**
	 * Method returns the number of wins a player has.
	 * 
	 * @param i int index of player
	 * 
	 * @returnmyNumWins String of number wins a player has.
	 * 
	 * @author Tierney Irwin
	 */
	public String getNumberWins(int i)
	{
		String myNumWins;
		myNumWins = ""+myModel.getPlayer(i).getNumberWins();
		return myNumWins;
	}
	
	/**
	 * Method returns the name of a player based on index passed in.
	 * 
	 * @param i int index of player
	 * 
	 * @returnString name of player
	 * 
	 * @author Tierney Irwin
	 */
	public String getName(int i)
	{
		return myModel.getPlayer(i).getName();
	}
	
	/**
	 * Method returns the image icon from a player's hand based on index of card passed in.
	 * 
	 * @param player int index of player 
	 * @param index int index of card
	 * 
	 * @return ImageIcon image icon of card called upon
	 * 
	 * @author Tierney Irwin
	 */
	public ImageIcon getImage(int player,int index)
	{
		ImageIcon myCardImageIcon = new ImageIcon(myModel.getPlayer(player).getHand().getCards().get(index).getImage());
		return myCardImageIcon;
	}
}
