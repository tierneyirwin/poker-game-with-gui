package mvc;
/**
 * The View class implements Swing to 
 * create an interface for the poker 
 * program to be played through.
 * 
 * @author Tierney Irwin
 * 
 * Due Date: 31 March 2016
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.MouseListener;
import java.io.File;
import java.lang.reflect.Method;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

public class View
{

	private Controller myController;
	public int myCounter=0;
    public final static int myNumSquares = 5;
    private JPanel myPlayerPanel;
    private JPanel myComputerPanel;
    private ButtonListener[] myCardListener;
	private JLabel[] myCards;
	private JLabel[] myComputerCards;
	private ButtonListener myGameButtonListener;
	private JButton myOneButton;
	public JLabel mySecondUpdateLog;
	public JLabel myLog;
	public JLabel myLogHandRanking;
	public JLabel myPlayer;
	public JLabel myComputer;
	public JLayeredPane myBackgroundPane;
	public JFrame myFrame1 = new JFrame("The pokey-est of Poker Games");
    
	/**
	 * View Constructor holds all the set up for the entire interface, 
	 * including the frame to the images of the cards.
	 * 
	 * @param controller Controller that is used to access the poker program
	 */
	public View(Controller controller) 
	{
		myFrame1.setResizable(false);
		myFrame1.setSize(900, 600);
		myBackgroundPane = new JLayeredPane();
		myBackgroundPane.setBackground(Color.lightGray);
		myBackgroundPane.setOpaque(true);

		
		GridBagLayout myLayout = new GridBagLayout();
		myBackgroundPane.setLayout(myLayout);
		myCards = new JLabel[myNumSquares];
		myComputerCards = new JLabel[myNumSquares];
		myCardListener = new ButtonListener[myNumSquares];
		myController=controller;
		
		
		myOneButton = new JButton("Start Game");
		Font font = new Font("Italic",Font.HANGING_BASELINE, 12);
		myOneButton.setFont(font);
		addComponent(myBackgroundPane,myOneButton,3,2,1,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH);

		/*
		 * This section adds all the images to the panels that hold the player and computer's hands.
		 */
		myPlayerPanel = new JPanel();
		myPlayerPanel.setBackground(Color.darkGray);
		myComputerPanel = new JPanel();
		myComputerPanel.setBackground(Color.darkGray);
        for(int i=0; i < myNumSquares; i++)
        {
           Image cardImage = myController.getImage(0, i).getImage();
           Image cardImage1 = myController.getImage(1, i).getImage();
           myCards[i] = new JLabel(new ImageIcon(cardImage));
           myComputerCards[i] = new JLabel(new ImageIcon(cardImage1));
           myPlayerPanel.add(myCards[i]);
           myComputerPanel.add(myComputerCards[i]);
        } 
        
        addComponent(myBackgroundPane,myPlayerPanel,0,3,5,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH);
        addComponent(myBackgroundPane,myComputerPanel,0,1,5,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH);

        /**
         * Displays the player's and computer's scores using JLabels in the corners.
         */
		String playerValue = myController.getNumberWins(0);
		String compValue = myController.getNumberWins(1);
		String playerName = myController.getName(0);
		myPlayer = new JLabel(playerName+": "+playerValue);
		addComponent(myBackgroundPane,myPlayer,4,0,1,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH);
		String computerName =  myController.getName(1);
		myComputer = new JLabel(computerName+": "+compValue);
		addComponent(myBackgroundPane,myComputer,0,0,1,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH);
		
		/**
		 * These JLabels act to show the status of who's winning, 
		 * the log of the game, and the status of your hand.
		 */
		mySecondUpdateLog = new JLabel("Just...letting you know...");
		addComponent(myBackgroundPane,mySecondUpdateLog,0,5,5,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH);
		myLog = new JLabel("HEY YOU! This is where I post stuff about the game.");
		addComponent(myBackgroundPane,myLog,0,4,5,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH);
		myLogHandRanking = new JLabel("Your Hand is: nothing, because there is nothing going on...duh");
		addComponent(myBackgroundPane,myLogHandRanking,4,4,2,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH);
		

		this.associateListeners();
		myFrame1.add(myBackgroundPane);
		myFrame1.setVisible(true);
		myFrame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Method connects the listeners for toggleSelect 
	 * and the button to the methods of the controller 
	 * class they should activate.
	 * 
	 * @author Tierney Irwin
	 */
	   public void associateListeners()
	    {
	        Class<? extends Controller> controllerClass;
	        Method toggleSelected,startGameMethod;
	        Class<?>[] classArgs;

	        controllerClass = myController.getClass();
	        
	        toggleSelected=null;
	        startGameMethod=null;
	        
	        classArgs = new Class[1];
	        
	        try
	        {
	           classArgs[0] = Class.forName("java.lang.Integer");
	        }
	        catch(ClassNotFoundException e)
	        {
	           String error;
	           error = e.toString();
	           System.out.println(error);
	        }
	        try
	        {
	           toggleSelected = controllerClass.getMethod("toggleSelected",classArgs);
	           startGameMethod = controllerClass.getMethod("startOrDiscard",(Class<?>[])null);
	        }
	        catch(NoSuchMethodException exception)
	        {
	           String error;

	           error = exception.toString();
	           System.out.println(error);
	        }
		     catch(SecurityException exception)
	        {
	           String error;

	           error = exception.toString();
	           System.out.println(error);
	        }
	        
	        int i;
	        Integer[] args;
	        myGameButtonListener = new ButtonListener(myController,startGameMethod,null);
	        myOneButton.addMouseListener(myGameButtonListener);

	        for (i=0; i < myNumSquares; i++)
	        {
	           args = new Integer[1];
	           args[0] = new Integer(i);
	           myCardListener[i] = 
	                   new ButtonListener(myController, toggleSelected, args);
	           myCards[i].addMouseListener(myCardListener[i]);
	        }
	    }
	   
	/**
	 * Method allows for easier use of GridBagLayout.
	 * 
	 * @author J. Eckroth
	 * 
	 * :D
	 */
    private static final Insets insets = new Insets(0, 0, 0, 0);
    private static void addComponent(
           Container container, Component component,
           int gridx, int gridy, int gridwidth, int gridheight,
           int anchor, int fill) 
    	{	
           
        GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, gridwidth, gridheight,
            1.0, 1.0, anchor, fill, insets, 0, 0);
            
        container.add(component, gbc);
        }
    
    /**
     * Method flips the player's cards to be seen.
     * 
     * @author Tierney Irwin
     */
    public void shownCards()
    {

    	 for(int i=0; i < myNumSquares; i++)
         {
    		 if(myController.myModel.getPlayer(0).getHand().getCards().get(i).isFaceUp()==false)
    		{
    			myController.myModel.getPlayer(0).getHand().getCards().get(i).flip();
   			}
            myCards[i].setIcon(myController.getImage(0, i));
            myPlayerPanel.add(myCards[i]);
         } 
    }
    /**
     * Method flips the computer's cards.
     * 
     * @author Tierney Irwin
     */
    public void shownCompCards()
    {

    	 for(int i=0; i < myNumSquares; i++)
         {

    			 myController.myModel.getPlayer(1).getHand().getCards().get(i).flip();
    		 
            myComputerCards[i].setIcon(myController.getImage(1, i));
            myComputerPanel.add(myComputerCards[i]);
         } 
    }
    /**
     * Method toggles the image if it is selected.
     * 
     * @param index int index of mySquare panels that are to be moved.
     * @param value boolean that shows if card is selected or deselected.
     * 
     * @author Tierney Irwin
     */
    public void toggleImage(int index,boolean value)
    {
    	if(value==true)
    	{
    		myCards[index].setSize(myCards[index].getWidth(), myCards[index].getHeight()-10);

    	}
    	else
    	{
    		myCards[index].setSize(myCards[index].getWidth(), myCards[index].getHeight()+10);
    	}
    	
    }
    /**
     * Method returns the text of the startGame button
     * 
     * @return String text of button
     * 
     * @author Tierney Irwin
     */
    public String getTitle()
    {
    	return myOneButton.getText();
    }
    /**
     * Method sets the text of the startGame button.
     * 
     * @param value String text to be the button's text
     * 
     * @author Tierney Irwin
     */
    public void setTitle(String value)
    {
    	myOneButton.setText(value);
    }
    
    /**
     * Method sets any JLabel to the text passed through the parameters.
     * 
     * @param button JLabel that's text will be changed
     * @param value String text to become the JLabel's text.
     * 
     * @author Tierney Irwin
     */
    public void setLog(JLabel button, String value)
    {
    	button.setText(value);
    }
    

}
